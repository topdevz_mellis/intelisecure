<pre><?php

// you know what to do!
define('DEBUG_MODE', 'true');

require "lib/plugin_base.class.php";
require "lib/logger.php";

date_default_timezone_set('UTC');

class is_loader {

    public $plugins = [];
    public $db = false;

    public function __construct() {

        logText("Initializing plugin loader");

        // set our default plugin dir if its not yet set
        if (!defined('PLUGIN_DIR')) {
            define('PLUGIN_DIR', getcwd() . "/plugins");
        }

        logText("Using plugin directory: " . PLUGIN_DIR);

        // connect to our db, or fail, because we cant do anything without it
        $this->db = new PDO("mysql:host=localhost;dbname=intelisecure;", "mcecreations", "") or die("Unable to connect to the database.");
                    //new PDO("mysql:host=localhost;dbname=intelisecure;","devuser","devpassword") or die("Unable to connect to the database.");

        logText("Loading plugins ..");
        $this->loadPlugins();

        // check if needs to be run

        $this->runPlugins();
    }

    public function loadPlugins() {

        // scan our plugin directory for files, and load them in
        $plugin_files = scandir(PLUGIN_DIR);
        foreach ($plugin_files as $file) {

            // ignore dotfiles
            if(substr($file, 0, 1) == ".") continue;

            $class = substr($file, 0, -4);
            logText("Attempting to load: " . $class);

            $real_path = PLUGIN_DIR . "/" . $file;
            include_once($real_path);

            if(class_exists($class)) {

                $this->plugins[] = new $class($this, $class);

            } else {
                logText("Failed to initialize plugin: " . $file);
            }
        }

    }

    public function runPlugins($override = false) {

        logText("Launching plugins");

        foreach ($this->plugins as $plugin) {

            $name = $plugin->getName();

            if(!$plugin->isLoaded()) {
                logText("Plugin isn't fully loaded yet: " . $name);
                continue;
            }

            $stmt = $this->db->prepare("SELECT * FROM plugins WHERE plugin_name=:name");
            $stmt->execute(['name' => $name]);
            $data = $stmt->fetch();

            if ($data !== false) {
                $date = new Datetime('now');
                $id = $data["id"];
                $stmt = $this->db->prepare("INSERT INTO run_history(plugin_id,date_run,num_records) VALUES (:id,:date_run,:num_records)");

                $plugin->run();

                $stmt->execute(array(
                                   "id" => $id,
                                   "date_run" => $date->format('Y-m-d H:i:s'),
                                   "num_records" => 1
                               ));
            }

        }

    }
}

// instantiate our loader class
$app = new is_loader();
?>


