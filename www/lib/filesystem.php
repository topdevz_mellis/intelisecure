<?php
class filesystem {

    public function ensureDirectoryExists($directory) {
        if (is_dir($directory)) {
            return;
        }
        if (file_exists($directory)) {
            throw new RuntimeException($directory.' exists and is not a directory.');
        }
        if (!@mkdir($directory, 0777, true)) {
            throw new RuntimeException($directory.' does not exist and could not be created.');
        }
    }


    public function fileExists($file) {
        return is_file($file) && is_readable($file);
    }


    public function getFileContents($path) {
        if (!$this->fileExists($path)) {
            throw new InvalidArgumentException(sprintf('File %s does not exist.', $path));
        }
        return file_get_contents($path);
    }


    public function putFileContents($path, $content) {
        $this->ensureDirectoryExists(\dirname($path));
        file_put_contents($path, $content);
    }


    public function createBackup($file) {
        if (!$this->fileExists($file)) {
            throw new RuntimeException('Could not create a backup from a non existing file: '.$file);
        }
        $backupFile = preg_replace('{\.backup$}', '', $file).'.backup';
        copy($file, $backupFile);
    }


    public function removeBackup($file) {
        $backupFile = preg_replace('{\.backup$}', '', $file).'.backup';
        if (!$this->fileExists($backupFile)) {
            return;
        }
        unlink($backupFile);
    }
}
?>