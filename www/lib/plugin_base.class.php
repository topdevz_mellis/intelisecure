<?php

class plugin_base {

    public $plugin_name;
    public $base_app;
    public $is_loaded = false;
    public $db = false;
    public $soap_config = array(
        // Stuff for development.
        'trace' => 1,
        'exceptions' => true,
        'cache_wsdl' => WSDL_CACHE_NONE,
        'features' => SOAP_SINGLE_ELEMENT_ARRAYS,

        // Auth credentials for the SOAP request.
        'login' => '',
        'password' => '',

        // Proxy url.
        'proxy_host' => '',
        'proxy_port' => 44300,

        // Auth credentials for the proxy.
        'proxy_login' => NULL,
        'proxy_password' => NULL,
    );

    function __construct($base_app, $plugin_name) {

        logText("Plugin init (" . $plugin_name . ")");

        // setup our base vars
        $this->base_app = $base_app;
        $this->plugin_name = $plugin_name;
        $this->db = $this->base_app->db;

        $this->register();

//        $created = gmdate('Y-m-d\TH:i:s\Z');
//        $nonce = mt_rand();
//        $passdigest = base64_encode( pack('H*', sha1( pack('H*', $nonce) . pack('a*',$created).  pack('a*',$pass))));
//
//        $auth = new stdClass();
//        $auth->Username = new SoapVar($user, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
//        $auth->Password = new SoapVar($pass, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
//        $auth->Nonce = new SoapVar($passdigest, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wss_ns);
//        $auth->Created = new SoapVar($created, XSD_STRING, NULL, $this->wss_ns, NULL, $this->wsu_ns);
//
//        $username_token = new stdClass();
//        $username_token->UsernameToken = new SoapVar($auth, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns);
//
//        $security_sv = new SoapVar(
//            new SoapVar($username_token, SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'UsernameToken', $this->wss_ns),
//            SOAP_ENC_OBJECT, NULL, $this->wss_ns, 'Security', $this->wss_ns);
//        parent::__construct($this->wss_ns, 'Security', $security_sv, true);

    }

    function getName() {
        return $this->plugin_name;
    }

    function isLoaded() {
        return $this->is_loaded;
    }

    function run() {
        logText("Running plugin: " . $this->getName());

        $this->plugin_run();
    }

    function register() {
        $stmt = $this->db->prepare("SELECT * FROM plugins WHERE plugin_name=:name");
        $stmt->execute(['name' => $this->plugin_name]);
        $data = $stmt->fetch();

        // if our plugin doesnt exist in the db, lets register it automatically
        if ($data == false) {
            logText("Registering plugin automatically: " . $this->plugin_name);

            $date = new Datetime('now');
            $stmt = $this->db->prepare("INSERT INTO plugins(plugin_name,date_reg) VALUES (:name,:date)");
            $stmt->execute(array(
                               "name" => $this->plugin_name,
                               "date" => $date->format('Y-m-d H:i:s')
                           ));
        }

        $this->is_loaded = true;
    }

    function insert_incident($id, $data) {
        $stmt = $this->db->prepare("SELECT * FROM incidents WHERE incident_id=:id");
        $stmt->execute(['id' => $id]);
        $data = $stmt->fetch();

        if ($data == false) {
            logText("Inserting incident: " . $id);

            $date = new Datetime('now');

            $values = "";
            foreach($data as $var => $val) {
                $values .= ":{$var}";
            }

            $stmt = $this->db->prepare("INSERT INTO incidents VALUES ({$values})");
            $stmt->execute($data);
        }
    }

    function modify_incident($id, $data) {
        $stmt = $this->db->prepare("SELECT * FROM incidents WHERE incident_id=:id");
        $stmt->execute(['id' => $id]);
        $data = $stmt->fetch();

        if ($data !== false) {
            logText("Updating incident: " . $id);

            $date = new Datetime('now');

            $values = "";
            foreach($data as $var => $val) {
                $values .= ":{$var}";
            }

            $stmt = $this->db->prepare("UPDATE incidents VALUES ({$values})");
            $stmt->execute($data);
        }
    }

    public function ensureDirectoryExists($directory) {
        if (is_dir($directory)) {
            return;
        }
        if (file_exists($directory)) {
            throw new RuntimeException($directory.' exists and is not a directory.');
        }
        if (!@mkdir($directory, 0777, true)) {
            throw new RuntimeException($directory.' does not exist and could not be created.');
        }
    }


    public function fileExists($file) {
        return is_file($file) && is_readable($file);
    }


    public function getFileContents($path) {
        if (!$this->fileExists($path)) {
            throw new InvalidArgumentException(sprintf('File %s does not exist.', $path));
        }
        return file_get_contents($path);
    }


    public function putFileContents($path, $content) {
        $this->ensureDirectoryExists(\dirname($path));
        file_put_contents($path, $content);
    }


    public function createBackup($file) {
        if (!$this->fileExists($file)) {
            throw new RuntimeException('Could not create a backup from a non existing file: '.$file);
        }
        $backupFile = preg_replace('{\.backup$}', '', $file).'.backup';
        copy($file, $backupFile);
    }


    public function removeBackup($file) {
        $backupFile = preg_replace('{\.backup$}', '', $file).'.backup';
        if (!$this->fileExists($backupFile)) {
            return;
        }
        unlink($backupFile);
    }
}

?>