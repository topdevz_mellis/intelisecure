<?php

class symantec_dlp extends plugin_base {

    public $last_date = false;

    function plugin_run() {
        $this->incident_url = 'https://10.129.127.10/ProtectManager/services/v2011/incidents';
        $this->last_date = strtotime("yesterday");

        $this->create_client();
        $this->get_incidents();
    }

    function create_client() {
        libxml_disable_entity_loader(false);

        $opts = array(
            'ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true)
        );

        $this->client = new SoapClient($this->incident_url, array(
            'login' => 'mellis:IS-WEB.NET',
            'password' => 'T0pD3vz!',
            'encoding' => 'UTF-8',
            'verifypeer' => false,
            'verifyhost' => false,
            'soap_version' => SOAP_1_1,
            'trace' => 1,
            'exceptions' => false,
            'connection_timeout' => 60,
            'stream_context' => stream_context_create($opts)));
    }

    function get_incidents() {
        $incidents = $this->client->__soapCall('incidentList', array(
            'incidentListRequest' =>
                array('savedReportId' => 4242,
                      'incidentCreationDateLaterThan' => $this->last_date)
        ));


        foreach ($incidents->incidentId as $incident) {

            $details = $this->client->__soapCall('incidentDetail', array(
                'incidentDetailRequest' => array(
                    'incidentId' => $incident,
                    'includeViolations' => true,
                    'includeHistory' => true,
                    'includeImageViolations' => false)));


            // debug dump
            //file_put_contents("./symantec_incidents.txt", var_dump($details) . '\n', FILE_APPEND);

            foreach($details as $incident) {

                $data = ['creation_date' => $incident->creationDate,
                         'detection_date' => $incident->detectionDate,
                         'severity' => $incident->severity,
                         'status' => $incident->status,
                         'source' => $incident->messageSource,
                         'type' => $incident->messageType,
                         'policy_name' => $incident->policyName,
                         'policy_label' => $incident->policyLabel,
                         'blocked_status' => $incident->blockedStatus,
                         'identifier' => $incident->originatorIdentifier,
                         'environment' => $incident->networkEnvironment,
                         'subject' => $incident->subject
                ];

                $this->insert_incident($incident->incidentId, $data);

            }
        }
    }
}

//register_plugin('symantec', 'get_incidents');
?>