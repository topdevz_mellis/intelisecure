
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `config_param` varchar(32) DEFAULT NULL,
  `config_value` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;

INSERT INTO `config` (`id`, `config_param`, `config_value`)
VALUES
	(1,'last_run',NULL),
	(2,'timeout','3600');

/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table incidents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `incidents`;

CREATE TABLE `incidents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `incident_id` int(11) DEFAULT NULL,
  `creation_date` timestamp NULL DEFAULT NULL,
  `detection_date` timestamp NULL DEFAULT NULL,
  `severity` varchar(10) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `source` varchar(11) DEFAULT NULL,
  `type` varchar(11) DEFAULT NULL,
  `policy_name` varchar(32) DEFAULT NULL,
  `policy_label` varchar(32) DEFAULT NULL,
  `blocked_status` varchar(11) DEFAULT NULL,
  `identifier` varchar(11) DEFAULT NULL,
  `environment` varchar(32) DEFAULT NULL,
  `subject` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plugins`;

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_name` varchar(30) DEFAULT NULL,
  `plugin_path` varchar(255) DEFAULT NULL,
  `date_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_run` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `plugins` WRITE;
/*!40000 ALTER TABLE `plugins` DISABLE KEYS */;

INSERT INTO `plugins` (`id`, `plugin_name`, `plugin_path`, `date_reg`, `last_run`)
VALUES
	(4,'symantec_dlp',NULL,'2018-08-24 14:50:50',NULL);

/*!40000 ALTER TABLE `plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table run_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `run_history`;

CREATE TABLE `run_history` (
  `run_id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin_id` int(11) NOT NULL,
  `date_run` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `num_records` int(11) DEFAULT NULL,
  PRIMARY KEY (`run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `run_history` WRITE;
/*!40000 ALTER TABLE `run_history` DISABLE KEYS */;

INSERT INTO `run_history` (`run_id`, `plugin_id`, `date_run`, `num_records`)
VALUES
	(1,4,'2018-08-24 14:59:21',1);

/*!40000 ALTER TABLE `run_history` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
